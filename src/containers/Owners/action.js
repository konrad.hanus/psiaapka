export const ADD_OWNER_TO_USER = '@@KEEPERS/ADD_OWNER_TO_USER';
export const REMOVE_USERS_OWNER = '@@KEEPERS/REMOVE_USERS_OWNER';
export const FETCH_USERS_OWNER = '@@KEEPERS/FETCH_USERS_OWNER';
export const PUT_OWNER_TO_USER = '@@KEEPERS/PUT_OWNER_TO_USER';

export const actionCreators = {
    addOwnerToUser: (owner) => {
        return ({
            type: ADD_OWNER_TO_USER,
            payload: owner
        })
    },
    putAllUsersOwners: (owner) => {
        return ({
            type: PUT_OWNER_TO_USER,
            payload: owner
        })
    },
    removeUsersOwner: (owner) => {
        return ({
            type: REMOVE_USERS_OWNER,
            payload: owner
        })
    },
    fetchAllOwnersOfUser: () => {
        return ({
            type: FETCH_USERS_OWNER,
        })
    },
    
};