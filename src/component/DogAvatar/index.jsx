
import {Pressable} from 'react-native';
import { Avatar } from "native-base";
import dogAvatar from './dog.jpeg';

const DogAvatar = (props) => {
    return (<Pressable
        onPress={() => props.onPress()}
        style={{
          width: 100,
          height: 100,
           position: "absolute",
          left: 70,
          bottom: 35,
          zIndex: 99,
        }}
      >
        <Avatar
          bg="amber.500"
          size="md"
          style={{
            borderWidth: 2,
            borderColor: "#d0deec",
          }}
          source={{uri: "https://scontent.fmla3-1.fna.fbcdn.net/v/t31.18172-8/12898159_991892777546984_4372972568906749017_o.jpg?_nc_cat=100&ccb=1-5&_nc_sid=cdbe9c&_nc_ohc=P_DdULl6UloAX_4Z482&_nc_ht=scontent.fmla3-1.fna&oh=00_AT8efZIaZEtdQHtA7Z2PNSlXZwb7woU0O9tpk8RivE90Tg&oe=6287DDB9"}}
        >
          TS
        </Avatar>
      </Pressable>);
}

export default DogAvatar;
