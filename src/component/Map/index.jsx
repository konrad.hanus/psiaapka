import React, { useEffect, useState, useRef } from "react";
import { StatusBar } from "expo-status-bar";
import {
  View,
  StyleSheet,
  SafeAreaView,
  Dimensions,
  ImageBackground,
} from "react-native";
import { Button } from "native-base";
import MapView, { Circle } from "react-native-maps";
import mapStyle from "../mapStyle";
import {
  PanGestureHandler,
  PanGestureHandlerGestureEvent,
  PinchGestureHandler,
  PinchGestureHandlerGestureEvent,
  GestureHandlerRootView,
} from "react-native-gesture-handler";
import Animated, {
  interpolate,
  useAnimatedGestureHandler,
  useAnimatedStyle,
  useSharedValue,
} from "react-native-reanimated";
import cloud from "./hC1.gif";
import { Avatar, NativeBaseProvider, Progress, Modal, ScrollView} from "native-base";
import { Entypo, FontAwesome } from "@expo/vector-icons";
import RightArrow from '../Right';
import CenterPointer from "../CenterPointer";
import LeftArrow from "../Left";
import TopArrow from "../TopArrow";
import BottomArrow from "../BottomArrow";
import BgTracking from "../GeolocationBackground";
import Menu from '../Menu';
import ProfileAvatar from '../ProfileAvatar';
import DogAvatar from "../DogAvatar";
import MenuRight from '../MenuRight';
import Horizont from '../Horizont';
import * as Location from "expo-location";
import { getDistanceFromLatLonInKm } from '../../helpers/getDistanceFromLatLonInKm';
// import DDD from '../DDD';

// import Geolocation from 'react-native-geolocation-service';
const { width: SCREEN_WIDTH, height: SCREEN_HEIGHT } = Dimensions.get("window");

export const Map = () => {

  const [permission, setPermission] = useState(false);
  const [zoom, setZoom] = useState(252);
  const [userLocation, setUserLocation] = useState();
  const [angle, setAngle] = useState(0);
  const [isRotating, setIsRotating] = useState(false);

  const defaultLocation = {
    latitude: 35.949309372017225,
    longitude: 14.406474545775206,
    latitudeDelta: 0.0022,
    longitudeDelta: 0.021,
  };

  const [locationMy, setLocationMy] = useState(defaultLocation);
  const [history, setHistory] = useState([]);
  const [distance, setDistance] = useState(0);

  // const [rotateS, setRotateS] = useState();
  // const [translateYS, setTranslateYS] = useState()

  // const addPerrmisions = async () => {
  //   console.log('jestem')
  //   try{
  //     console.log('tu');
  //     const foregroundPermission = await Location.requestForegroundPermissionsAsync();
  //     console.log(foregroundPermission);
  //   if(foregroundPermission.granted === true)
  //   {
  //     setPermission(true);
  //   }
 
  //   // Location subscription in the global scope
  //   let locationSubscrition = null

  //   // Locatoin tracking inside the component
  //   if (foregroundPermission.granted) {
  //     foregroundSubscrition = Location.watchPositionAsync(
  //       {
  //         // Tracking options
  //         accuracy: Location.Accuracy.High,
  //         distanceInterval: 10,
  //       },
  //       (currentLocation) => {
  //         //  console.log('lokacja, w tle', currentLocation.coords);

  //         setLocationMy((prev) => ({
  //           ...prev,
  //           latitude: currentLocation.coords.latitude,
  //           longitude: currentLocation.coords.longitude
  //         }));

  //         setHistory((prev) => {
  //           // console.log('history', prev);
  //           setDistance((prevDistance)=> {
  //             // console.log('distance', prevDistance)
  //             if(prev.length === 0){
  //               return 0;
  //             }

  //             const latestItem = prev[prev.length - 1];

  //             return(prevDistance + getDistanceFromLatLonInKm(
  //               latestItem.latitude, 
  //               latestItem.longitude, 
  //               currentLocation.coords.latitude,
  //               currentLocation.coords.longitude))

  //           });
            
  //           if(prev)
  //           {
  //             return prev.concat({
  //               latitude: currentLocation.coords.latitude,
  //               longitude: currentLocation.coords.longitude,
  //               speed: currentLocation.coords.speed,
  //             });
  //           }
  //         })
  //       }
  //     );
     
  //   }
  //   }catch (error) {
  //     alert('cos z uprawnieniami');
  //   }
  // };

  // useEffect(() => {
  //   addPerrmisions();

  //   if(permission)
  //   {
  //     setTimeout(() => handleGesture(252), 500);
  //   }
  //   // console.log("zmienione");
    
  // }, []);

  const image = cloud;
  const [location, setLocation] = useState({
    latitude: 35.949309372017225,
    longitude: 14.406474545775206,
    latitudeDelta: 0.0022,
    longitudeDelta: 0.021,
  });

  const onRegionChangeComplete = (x) => {
    setLocation(x);
  };
  const onRegionChange = (x) => {
    return {
      latitude: 35.949309372017225,
      longitude: 14.406474545775206,
      latitudeDelta: 0.0022,
      longitudeDelta: 0.021,
    };
  };
  const loc = {
    longitude: 35.949309372017225,
    latitude: 14.406474545775206,
  };

  const renderPosition = () => {
    return (
      <MapView.Marker
        coordinate={{
          latitude: location.latitude+0.001,
          longitude: location.longitude+0.001,
        }}
        style={{
          borderWidth: 1,
          borderColor: "black",
          backgroundColor: "yellow",
        }}
      >
        {/* <Image source={require('./smog.png')} resizeMode="stretch" style={{height: 100, position: 'absolute', top: 0}} />  */}

        {/* <View
          style={{ width: 30, height: 30, backgroundColor: "red" }}
          onPress={() => alert("test")}
        /> */}
         {/* <DDD />  */}
      </MapView.Marker>
    );
  };

  const item1 = () => {
    return (
      <MapView.Marker
        coordinate={{
          latitude: location.latitude+0.001,
          longitude: location.longitude+0.001,
        }}
        style={{
          borderWidth: 1,
          borderColor: "black",
          backgroundColor: "yellow",
        
          
        }}
      >
        {/* <View style={{height: 100, width: 100, position: 'absolute', top: 0}}> */}
        <View style={{ height: 30, width: 30, borderColor: 'black', backgroundColor: 'white', borderRadius: 20 }} />
        {/* </View> */}
        {/* <View
          style={{ width: 30, height: 30, backgroundColor: "red" }}
          onPress={() => alert("test")}
        /> */}
         {/* <DDD />  */}
      </MapView.Marker>
    );
  };

  const item2 = () => {
    return (
      <MapView.Marker
        coordinate={{
          latitude: location.latitude-0.001,
          longitude: location.longitude+0.001,
        }}
        style={{
          borderWidth: 1,
          borderColor: "black",
          backgroundColor: "yellow",
        }}
      >
        {/* <Image source={require('./smog.png')} resizeMode="stretch" style={{height: 100, position: 'absolute', top: 0}} />  */}

        {/* <View
          style={{ width: 30, height: 30, backgroundColor: "red" }}
          onPress={() => alert("test")}
        /> */}
         {/* <DDD />  */}
      </MapView.Marker>
    );
  };

  const item3 = () => {
    return (
      <MapView.Marker
        coordinate={{
          latitude: location.latitude+0.001,
          longitude: location.longitude-0.001,
        }}
        style={{
          borderWidth: 1,
          borderColor: "black",
          backgroundColor: "yellow",
        }}
      >
        {/* <Image source={require('./smog.png')} resizeMode="stretch" style={{height: 100, position: 'absolute', top: 0}} />  */}

        {/* <View
          style={{ width: 30, height: 30, backgroundColor: "red" }}
          onPress={() => alert("test")}
        /> */}
         {/* <DDD />  */}
      </MapView.Marker>
    );
  };

  const item4 = () => {
    return (
      <MapView.Marker
        coordinate={{
          latitude: location.latitude-0.001,
          longitude: location.longitude-0.001,
        }}
        style={{
          borderWidth: 1,
          borderColor: "black",
          backgroundColor: "yellow",
        }}
      >
        {/* <Image source={require('./smog.png')} resizeMode="stretch" style={{height: 100, position: 'absolute', top: 0}} />  */}

        {/* <View
          style={{ width: 30, height: 30, backgroundColor: "red" }}
          onPress={() => alert("test")}
        /> */}
         {/* <DDD />  */}
      </MapView.Marker>
    );
  };

  const translateY = useSharedValue(0);
  const mapRef = useRef(null);

  const handleGesture = (evt) => {
    // let {nativeEvent} = evt
    // console.log('aaa', nativeEvent.translationX);

    // onClickButton(nativeEvent.translationY)
    // translateY.value = nativeEvent.translationY
    onClickButton(evt);
    translateY.value = evt;
  };

  const handleRotateButton = async (x) => {
    // console.log(x);
    // console.log("aaa", nativeEvent.translationX);
   
    setIsRotating(true);
    const camera = await mapRef.current.getCamera();
    
    // console.log('a', e);
    
    // camera = e;
    camera.heading += x
    mapRef.current.animateCamera(camera, { duration: 1000 })
    setIsRotating(false);
    
    setAngle(nativeEvent.translationX);

    // mapRef.current.setCamera({
    //     heading: angle + nativeEvent.translationX,
    //    });
    
      //  console.log(angle);
    // onClickButton(nativeEvent.translationY)
    // translateY.value = nativeEvent.translationY
    // onClickButton(evt)
    // translateY.value = evt
  };

  const handleGesture2 = async (evt) => {
    let { nativeEvent } = evt;
    // !isRotating && await 
    handleRotateButton(nativeEvent.translationX);
  };

  const sStyle = useAnimatedStyle(() => {
    const rotate = interpolate(translateY.value, [0, SCREEN_WIDTH / 2], [0, 5]);
   
    // onClickButton2(translateY.value)
    return {
      transform: [
        { perspective: 100 },
        {
          translateY: translateY.value - 600,
        },
        {
          rotateX: `-${rotate}deg`,
        },
      ],
    };
  }, []);

  const rStyle = useAnimatedStyle(() => {
    const rotate = interpolate(translateY.value, [0, SCREEN_WIDTH / 2], [0, 5]);
    // onClickButton2(translateY.value)
    return {
      transform: [
        { perspective: 100 },
        {
          translateY: translateY.value - 600,
        },
        {
          rotateX: `${rotate}deg`,
        },
      ],
    };
  }, []);

  const style = {
    transform: [
      { perspective: 100 },
      {
        translateY: 100 - 600,
      },
      {
        rotateX: `3.5deg`,
      },
    ],
  };

  // const rStyle = {};
  // console.log(rStyle);
  const onClickButton = (val) => {
    // console.log("click");
    mapRef.current.animateToRegion({
      latitude: 35.949309372017225,
      longitude: 14.406474545775206,
      latitudeDelta: 0.00092 + (val / 100000) * 3,
      longitudeDelta: 0.00041 + (val / 100000) * 3,
    });
  };

  const setCurrentUserLocation = (latitude, longitude) => {
    // console.log("click");
    // mapRef.current.animateToRegion({
    //   latitude: latitude,
    //   longitude: longitude,
    //   latitudeDelta: 0.00022,
    //   longitudeDelta: 0.0021,
    // });
    const current = new Date();
    const minutes = current.getSeconds();

    if(!isRotating && minutes%30)
    {
        mapRef.current.getCamera().then((e)=>
        { 
            console.log('a', e);
            let camera;
            camera = e;
            camera.center.latitude = latitude;
            camera.center.longitude = longitude;
            mapRef.current.animateCamera(camera, { duration: 200 })
        });
    }

  };

  const rotateLeft = () => {

     handleRotateButton(55);
    
    // console.log(zoom);
    
  };

  const zoomOut = () => {
     

     const maxZoom = 300;
    if(maxZoom >= zoom)
    {
        console.log('down', zoom);
        handleGesture(zoom + 12);
        setZoom(zoom + 12);
    }
  }

  const zoomIn = () => {
    const maxZoom = 108;
    if(maxZoom < zoom)
    {
      // console.log('up', zoom);
      handleGesture(zoom - 12);
      setZoom(zoom - 12);
    }
}

  const rotateRight = () => {
    handleRotateButton(-55);
    // setZoom(zoom - 12);
    // console.log(zoom);
    // handleGesture(zoom);
  };

  const setCurrent = () =>{
    setCurrentUserLocation(userLocation.latitude, userLocation.longitude)
  }

  const [modalVisible, setModalVisible] = React.useState(false);
  const [size, setSize] = React.useState("md");

  const handleSizeClick = newSize => {
    setSize(newSize);
    setModalVisible(!modalVisible);
  };

  // const currentLocationHandler = () => {
  //     // <!-- Get location coordinates using GeoLocation npm package -->
  //         let currentRegion = {
  //           latitude: Geolocation.latitude,
  //           longitude: Geolocation.longitude,
  //           latitudeDelta: 0.001,
  //           longitudeDelta: 0.001,
  //         };
  //         mapRef.current.animateToRegion(currentRegion, 3 * 1000);
  //     }

  
  return (
    permission ?
    <NativeBaseProvider>
      {/* <GestureHandlerRootView style={{ flex: 1 }}> */}
        <ImageBackground
          source={require("./tlo.png")}
          style={{
            width: "100%",
            height: 500,
            backgroundColor: "#000",
            position: "absolute",
            top: 0,
          }}
          imageStyle={{
            resizeMode: "cover",
            alignSelf: "flex-end",
          }}
        >
           <SafeAreaView style={styles.container}>
            <StatusBar style="inverted" />
          {/*  <PanGestureHandler
              style={{ position: "absolute" }}
              onGestureEvent={handleGesture2}
            >
              <View
                style={{
                  width: SCREEN_WIDTH,
                  height: SCREEN_HEIGHT,
                  position: "absolute",
                  zIndex: 98,
                  opacity: 1,
                }}
              >  
                <Animated.View
                  style={[
                    {
                      backgroundColor: "black",
                      width: SCREEN_WIDTH * 2,
                      height: SCREEN_HEIGHT * 2,
                      marginLeft: -SCREEN_WIDTH / 2,
                    },
                   rStyle,
                  ]}
                > */
                  <MapView
                    ref={mapRef}
                    style={{
                      width: SCREEN_WIDTH ,
                      height: SCREEN_HEIGHT ,
                      zIndex: -9991,
                    }}
                    initialRegion={{
                      latitude: 35.949309372017225,
                      longitude: 14.406474545775206,
                      latitudeDelta: 0.00092,
                      longitudeDelta: 0.00041,
                    }}
                    onUserLocationChange={(e) => {
                        setUserLocation(e.nativeEvent.coordinate);
                    }}
                    showsCompass={true}
                    provider={MapView.PROVIDER_GOOGLE}
                    customMapStyle={mapStyle}
                    onRegionChange={onRegionChange}
                    onRegionChangeComplete={onRegionChangeComplete}
                    zoomEnabled={true}
                    scrollEnabled={true}
                    showsScale={true}
                    rotateEnabled={true}
                    followsUserLocation={true}
                    showsUserLocation={true} 
                
                  /> /*}
                    {/* {renderPosition()} */}
                    {/* {item1()}
                    {item2()}
                    {item3()}
                    {item4()} */}
                    {/* <Circle
                      center={{
                        latitude: 35.949309372017225,
                        longitude: 14.406474545775206,
                      }}
                      radius={100}
                      fillColor="rgba(253, 48, 4,0.5)"
                      strokeColor="rgba(253, 48, 4,1)"
                    /> */}
                  {/* </MapView>
                  <Horizont />
                </Animated.View>
              </View>
            </PanGestureHandler> */}
            {/* <TopArrow zoomIn={zoomIn} />
            <BottomArrow zoomOut={zoomOut} />
            <RightArrow rotateRight={rotateLeft} />
            <CenterPointer setCurrent={setCurrent} />
            <LeftArrow rotateLeft={rotateRight} /> */}
            <ProfileAvatar onPress={rotateLeft} distance={distance} speed={history[history.length-1] ? history[history.length-1].speed: 0} />
            <DogAvatar onPress={rotateLeft} />
            <Menu handleSizeClick={handleSizeClick} modalVisible={modalVisible} setModalVisible={setModalVisible} />
            <MenuRight onPress={rotateLeft} />
          </SafeAreaView>
        </ImageBackground>
      {/* </GestureHandlerRootView> */}
    </NativeBaseProvider>
  : <NativeBaseProvider><View style={{position: 'absolute', top: 200, left: 100}}><Button onPress={()=>addPerrmisions()}>Włącz uprawnianie GPS</Button></View></NativeBaseProvider>);
};

export default Map;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //  backgroundColor: '#d0deec'
  },
  image: {
    width: "100%",
    justifyContent: "center",
    position: "absolute",
    top: 0,
  },
  smog: {
    position: "absolute",
    top: -100,
  },
});
