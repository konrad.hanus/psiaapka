import { takeEvery, call, put, all, fork, select } from 'redux-saga/effects';
import { PUT_MY_LOCATION } from './action';
import { reduxSagaFirebase } from '../Walk/saga';
import * as Device from 'expo-device';
import { actionCreators } from '../ShareLocationCounter/action';

function* onShareLocationSaga(action) {
    
    const getLocationCounter = yield select((state)=>state.locationCounter);

    const what = action.meta;

    if(what)
    {
            const location = action.payload;
            yield call(reduxSagaFirebase.database.create, '/public/'+what+'/', { location, who: Device.deviceName  });
            yield put(actionCreators.decreaseLocationCounter());
            const currentLocationCounter = yield select((state)=>state.locationCounter);
            // yield call(alert,"Moja lokalizacja została udostępniona pozostało: "+getLocationCounter);
    }else{
        
        if(getLocationCounter > 0)
        {
            const location = action.payload;
            yield call(reduxSagaFirebase.database.create, '/public/players/'+Device.deviceName+'/', { location });
            yield put(actionCreators.decreaseLocationCounter());
            const currentLocationCounter = yield select((state)=>state.locationCounter);
            yield call(alert,"Moja lokalizacja została udostępniona pozostało: "+getLocationCounter);
        }else{
            yield call(alert,"Nie masz juz flar, poszukaj ich razem z psem");
        }

    }
}

export function* shareLocationSaga() {
    yield takeEvery(PUT_MY_LOCATION, onShareLocationSaga);
}

export function* shareLocation() {
    yield all([fork(shareLocationSaga)]);
}
