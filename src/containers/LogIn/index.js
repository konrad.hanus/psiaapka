import React from 'react';
import {
	Body,
	Button,
	Container,
	Text,
	Form,
	Item,
	Label,
	Icon,
	Input, 
	Spinner, 

} from 'native-base';

import { connect } from 'react-redux';
import { actionCreators as actionCreatorsRegistrationForm } from './../RegistrationForm/action';
import { actionCreators } from './action';

export class LogIn extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			email: null,
			password: null
		};
	}

	componentDidMount() {
		this.props.rehydrateAuth({redirect: this.props.navigation.navigate});
	}

	render() {
		return (
			<Container>
				<Form>
					<Item floatingLabel>
						<Icon active name="account" type="MaterialCommunityIcons" />
						<Label>E-mail</Label>
						<Input onChangeText={(value) => this.setState({ email: value })} />
					</Item>
					<Item floatingLabel>
						<Icon active name="lock" type="MaterialCommunityIcons" />
						<Label>Hasło</Label>
						<Input secureTextEntry={true} onChangeText={(value) => this.setState({ password: value })} />
					</Item>
					<Button
						full success
						onPress={() =>
							this.props.login({
								email: this.state.email,
								password: this.state.password,
								redirect: this.props.navigation.navigate
							})}
					>
						<Text>Zaloguj się</Text>
					</Button>

					<Text> </Text>
					<Button full onPress={() => this.props.navigation.navigate('RegistrationForm')}>
						<Text>Zarejestruj się</Text>
					</Button>
					{this.props.spinner && <><Spinner /><Body><Text>logowanie...</Text></Body></>}
					{/* <Text> </Text>
					<Button onPress={()=>this.props.rehydrateAuth({redirect: this.props.navigation.navigate})}><Text>Zaloguj się ponownie </Text></Button> */}
				</Form>
			</Container>
		);
	}
}

export default connect(
	(state) => {
		return {
			auctions: state.auctions,
			spinner:  state.login.spinner 
		};
	},
	{
		rehydrateAuth: actionCreators.rehydrateAuth,
		presistAuth: actionCreators.presistAuth,
		login: actionCreatorsRegistrationForm.login
	}
)(LogIn);
