import { PUT_FRIENDS_LOCATION } from './action';

const initiaState = []

export function friendsReducer(state=initiaState, action){
    switch (action.type) {

        case PUT_FRIENDS_LOCATION:
          return action.payload;
    
        default:
          return state;
    };
   }
   
   export default friendsReducer;