import React, { Component } from 'react';
import {
    Content,
    Text,
    List, ListItem, Body,
    Container, Header, Item, Input, Icon, Button, Right,
} from 'native-base';

import {
    Alert,
    Modal,
    StyleSheet,
    TouchableHighlight,
    View,
    Image
} from "react-native";

import IconFontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { actionCreators } from './action';
import { actionCreators as keppersActionCreators } from '../Keepers/action';
import { actionCreators as ownersActionCreators } from '../Owners/action';
import { connect } from 'react-redux';
import IconFoundation from 'react-native-vector-icons/Foundation';

class Search extends Component {

    componentDidMount() {
        this.props.fetchAllUsers();
        this.props.fetchAllUsersKeepers();
        this.props.fetchAllOwnersOfUser();
    }

    state = {
        modalVisible: false,
        currentUser: null
    };

    onClickRemoveOwner = (keeper) => {
        this.props.removeUsersOwner(keeper);
    }

    onClickRemoveKeeper = (keeper) => {
        this.props.removeUsersKeeper(keeper);
    }

    onClickAddKeeper = () => {
        this.props.addKeepersToUser(this.state.currentUser);
        this.setModalVisible(false);
    }

    onClickAddOwner = () => {
        this.props.addOwnerToUser(this.state.currentUser);
        this.setModalVisible(false);
    }

    isEmpty = (array) =>{
       return typeof array !== 'undefined' && array.length > 0
    }

    setModalVisible = (visible, user) => {
        this.setState({ modalVisible: visible, currentUser: user });
    }

    isEmpty = (array) =>{
        return typeof array !== 'undefined' && array.length > 0
     }

    isMyKeeper = (id) => {
        if(id)
        {
            const value = Object.values(this.props.keepers).filter((keeper)=>{
                return keeper.id === id;
            });

            return this.isEmpty(value);
        }
        return false;
    }

    isMyOwner = (id) => {
        if(id)
        {
            const value = Object.values(this.props.owners).filter((owner)=>{
                return owner.id === id;
            });

            return this.isEmpty(value);
        }
        return false;
    }

    render() {
        const { modalVisible } = this.state;
        return (
            <Content>
                <Container>
                    <Header searchBar rounded>
                        <Item>
                            <Icon name="ios-search" />
                            <Input placeholder="e-mail" />
                            <Icon name="ios-people" />
                        </Item>
                        <Button transparent>
                            <Text>Szukaj</Text>
                        </Button>
                    </Header>
                    <List>
                        {this.props.users && this.props.users.map((user, id) => <ListItem key={id}>
                            <Body><Text>{user.email}</Text></Body>

                            <Right>
                                {this.isMyKeeper(user.id) ?
                                    <Button small danger onPress={() => {
                                        this.onClickRemoveKeeper(user);
                                    }}><Text><IconFontAwesome5 name="user-minus" size={15} /></Text></Button>:

                                    this.isMyOwner(user.id) ? 
                                    <Button small danger onPress={() => {
                                        this.onClickRemoveOwner(user);
                                    }}><Text><IconFoundation name="guide-dog" size={22} /></Text></Button> :
                                    <Button small onPress={() => {
                                        this.setModalVisible(true, user);
                                    }}><Text><IconFontAwesome5 name="user-plus" size={15} /></Text></Button>}
                        
                            </Right>
                        </ListItem>)
                        }
                    </List>

                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={modalVisible}
                        onRequestClose={() => {
                            Alert.alert("Modal has been closed.");
                        }}
                    >
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <IconFontAwesome5 name="question" size={40} />

                                <Text style={styles.modalText}>No i co mam teraz robić z</Text>
                                <Text style={styles.modalText}>{this.state.currentUser && this.state.currentUser.email}</Text>
                                <Button block success onPress={() => {
                                    this.onClickAddKeeper();
                                }}><Text>Dodaj osobę jako opiekuna</Text>
                                    <Text><IconFontAwesome5 name="user-plus" size={20} /></Text>
                                </Button>
                                <Text></Text>
                                <Button block onPress={() => {
                                    this.onClickAddOwner();
                                }}><Text>Wyprowadzajcie psa razem</Text>
                                    <Text><IconFoundation name="guide-dog" size={30} /></Text>
                                </Button>
                                <Text></Text>
                                <Button danger block onPress={() => {
                                    this.setModalVisible(!modalVisible);
                                }}>
                                    <Text><IconFontAwesome5 name="window-close" size={20} /></Text>
                                    <Text>Zamknij </Text>
                                </Button>

                            </View>
                        </View>
                    </Modal>
                </Container>


                {/* <Body>
                <Text> </Text>
                    <Text>Twój unikatowy nr:</Text>
                    <Text>{this.props.uid}</Text>
                    <Text> </Text>
                    <Image source={{uri: `https://api.qrserver.com/v1/create-qr-code/?size=250x250&data=${this.props.uid}`}} style={{height: 250, width: 250, flex: 1}}/>
                    <Text> </Text>
                    <Text>Udostępnij go znajomemu</Text>
                    <Text>z którą chcesz się połączyć</Text>
                    
                </Body> */}

            </Content>
        );
    }
}

export default connect(
    ((state) => {
        return ({
            uid: state.user && state.user.user.uid && state.user.user.uid,
            users: state.users,
            keepers: state.keepers,
            owners: state.owners
        })
    }), { ...actionCreators, ...keppersActionCreators, ...ownersActionCreators }
)(Search);

const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    openButton: {
        backgroundColor: "#F194FF",
        borderRadius: 20,
        padding: 10,
        elevation: 2
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center"
    }
});




