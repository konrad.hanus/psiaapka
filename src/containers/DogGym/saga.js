import { takeEvery, call, put, all, fork, select } from 'redux-saga/effects';
import { FETCH_DOG_GYMS_LOCATION, REMOVE_DOG_GYM_LOCATION, actionCreators } from './action';
import { reduxSagaFirebase } from '../Walk/saga';

function* onFetchDogGymsLocation() {
    const allDogsGyms = yield call(reduxSagaFirebase.database.read, '/public/dogGym/');
    yield put(actionCreators.putDogGymsLocation(allDogsGyms));
}

function* onRemoveDogGymLocation(action) {
    const allDogsGyms = yield call(reduxSagaFirebase.database.delete, '/public/dogGym/'+action.meta);
    alert('Dog Gym usunięto');
    yield put(actionCreators.fetchDogGymsLocation());
}

export function* fetchDogGymsLocation() {
    yield takeEvery(FETCH_DOG_GYMS_LOCATION, onFetchDogGymsLocation);
}

export function* removeDogGymLocation() {
    yield takeEvery(REMOVE_DOG_GYM_LOCATION, onRemoveDogGymLocation);
}

export function* dogGymsLocation() {
    yield all([fork(fetchDogGymsLocation), fork(removeDogGymLocation)]);
}
