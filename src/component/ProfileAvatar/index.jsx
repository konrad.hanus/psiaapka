
import {Pressable} from 'react-native';
import { Avatar, Progress, Text} from "native-base";
import profileAvatar from './profile.jpeg';

const ProfileAvatar = (props) => {

  const levelsKM = [0,1,3,7,12,22,42,67,117];
                 // 1km                // 2km                 // 4 km               // 5km              // 10km           // 20km                         // 25 km              // 50km           // 100km
  const levels = [props.distance*100, (props.distance-levelsKM[1])*50, (props.distance-levelsKM[2])*25, (props.distance-levelsKM[3])*20, (props.distance-levelsKM[4])*10, (props.distance-levelsKM[5])*5, (props.distance-levelsKM[6])*4, (props.distance-levelsKM[7])*2, (props.distance-levelsKM[8])]
    
  const getLevel = () =>{
    if(levels[0] > 100)
    {
      if(levels[1] > 100)
      {
        if(levels[2] > 100)
        {
          if(levels[3] > 100)
          {
            if(levels[4] > 100)
            {
              if(levels[5] > 100)
              {
                if(levels[6] > 100)
                {
                  if(levels[7] > 100)
                  {
                    return 8;
                  }
                  return 7;
                }
                return 6;
              }
              return 5;
            }
            return 4;
          }
          return 3;
        }
        return 2;
      }
      return 1
    }
    return 0;
  } 
  return (<><Pressable
        onPress={() => props.onPress()}
        style={{
          width: 100,
          height: 100,
          position: "absolute",
          left: 20,
          bottom: 50,
          zIndex: 99,
        }}
      >
        <Avatar
          bg="amber.500"
          size="lg"
          source={{uri: 'https://scontent.fmla3-1.fna.fbcdn.net/v/t1.18169-9/10406472_716193918450206_7253765191644962986_n.jpg?_nc_cat=106&ccb=1-5&_nc_sid=09cbfe&_nc_ohc=irZEZ7ISs8YAX-H6DAj&_nc_ht=scontent.fmla3-1.fna&oh=00_AT-l_JrZagbO9W8Uo9YmDwcU5AezyeWs1e6FfVtm5qN7-g&oe=6288D707'}}
          style={{
            borderWidth: 2,
            borderColor: "#d0deec",
          }}
        ></Avatar>
        <Progress shadow={2} colorScheme="emerald" value={levels[getLevel()]} mx="4" style={{position: 'aboslute', left: -15, top: 7, width: 90}} ></Progress>
      </Pressable>
      <Text style={{position: 'absolute', top: 50, right:50}}>Level {getLevel()}</Text>
      <Text style={{position: 'absolute', top: 70, right:50}}>Trip {(props.distance - levelsKM[getLevel()]).toFixed(2)} km </Text>
      <Text style={{position: 'absolute', top: 90, right:50}}>Sum {props.distance.toFixed(2)} km</Text>
      <Text style={{position: 'absolute', top: 110, right:50}}>{props.speed && props.speed > 0 ? props.speed.toFixed(2) :0} km/h</Text></>);
}

export default ProfileAvatar;
