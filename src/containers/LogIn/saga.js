import { takeEvery, put, all, fork, call } from 'redux-saga/effects';
import { REHYDRATE_AUTH, PRESIST_AUTH } from './action';
import { AsyncStorage } from 'react-native';
import { actionCreators } from './action';
import { actionCreators as actionCreatorsRegistrationForm } from '../RegistrationForm/action';

const USER_TOKEN = '@LOCAL_STORAGE/user';

export function* onRehydrateAuthSaga(action) {
	
	const redirect = action.payload.redirect;
	try {
		const response = yield call(AsyncStorage.getItem, USER_TOKEN);
		//why is used 2x json parse?
		const res = JSON.parse(JSON.parse(response));
		const all = { ...res, redirect };
		if (res !== null) {
			yield put(actionCreatorsRegistrationForm.login(all));
		}else{
			yield put(actionCreators.spinnerOff());
		}
		
	} catch (e) {
		console.log('error', e);
		yield put(actionCreators.spinnerOff());
	}
}

export function* onPresistAuthSaga(action) {
	const user = JSON.stringify(action.payload);
	try {
		const response = yield call(AsyncStorage.setItem, USER_TOKEN, JSON.stringify(user));
	} catch (e) {
		alert('Xerror', e);
		console.log('error', e);
	}
}

export function* rehydrateAuthSaga() {
	yield takeEvery(REHYDRATE_AUTH, onRehydrateAuthSaga);
}

export function* presistAuthSaga() {
	yield takeEvery(PRESIST_AUTH, onPresistAuthSaga);
}

export function* loginSaga() {
	yield all([ fork(rehydrateAuthSaga), fork(presistAuthSaga) ]);
}
