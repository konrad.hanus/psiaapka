import { PUT_ALL_USERS_KEEPERS } from './action';

const initiaState = []

export function keppersReducer(state=initiaState, action){
    switch (action.type) {

        case PUT_ALL_USERS_KEEPERS:
          return action.payload;
    
        default:
          return state;
    };
   }
   
   export default keppersReducer;