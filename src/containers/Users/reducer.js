import { PUT_ALL_USERS } from './action';

const initiaState = null

export function loginReducer(state=initiaState, action){
    switch (action.type) {

        case PUT_ALL_USERS:
          return action.payload;
    
        default:
          return state;
    };
   }
   
   export default loginReducer;