import { takeEvery, call, put, all, fork, select } from 'redux-saga/effects';
import { FETCH_DOG_SPOTS_LOCATION, REMOVE_DOG_SPOT_LOCATION, actionCreators } from './action';
import { reduxSagaFirebase } from '../Walk/saga';

function* onFetchDogSpotsLocation() {
    const allDogsSpot = yield call(reduxSagaFirebase.database.read, '/public/dogSpot/');
    yield put(actionCreators.putDogSpotsLocation(allDogsSpot));
}

function* onRemoveDogSpotLocation(action) {
    const allDogsSpot = yield call(reduxSagaFirebase.database.delete, '/public/dogSpot/'+action.meta);
    alert('Dog Spot usunięto');
    yield put(actionCreators.fetchDogSpotsLocation());
}

export function* fetchDogSpotsLocation() {
    yield takeEvery(FETCH_DOG_SPOTS_LOCATION, onFetchDogSpotsLocation);
}

export function* removeDogSpotLocation() {
    yield takeEvery(REMOVE_DOG_SPOT_LOCATION, onRemoveDogSpotLocation);
}

export function* dogSpotsLocation() {
    yield all([fork(fetchDogSpotsLocation), fork(removeDogSpotLocation)]);
}
