import { Header, Left, Icon, IconButton, Body, Title, Right, Fab } from "native-base";
import { Entypo, FontAwesome } from "@expo/vector-icons";

const LeftArrow = (props) => {
    return (<IconButton icon={<Icon as={Entypo} name="chevron-right" />} borderRadius="full" _icon={{
        color: "orange.500",
        size: "md"
    }} _hover={{
        bg: "orange.600:alpha.20"
    }} _pressed={{
        bg: "orange.600:alpha.20",
        _icon: {
            name: "emoji-flirt"
        },
        _ios: {
            _icon: {
                size: "2xl"
            }
        }
    }} _ios={{
        _icon: {
            size: "2xl"
        }
    }}
        onPress={() => props.rotateLeft()}
        style={{
            width: 100,
            height: 100,
            position: "absolute",
            right: 20,
            top: 130,
            zIndex: 99,
        }}
    />);
}

export default LeftArrow;