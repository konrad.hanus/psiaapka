import React, { Component } from 'react';
import { Content, Card, CardItem, Text, Body, Icon, Button, Left, Right } from 'native-base';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Fontisto from 'react-native-vector-icons/Fontisto';
import moment from 'moment';
import { AsyncStorage } from 'react-native';
import { WalkContext } from '../../context';
import { connect } from 'react-redux';
import { actionCreators as actionCreatorsWalk } from '../Walk/action';
import { actionCreators as testActionCreators } from '../Test/action';
import TimePicker from '../TimePicker';
class Walking extends Component {

    constructor(props) {
        super(props);

        this.state = {
            start: this.props.walk.start ? this.props.walk.start : 0,
            end: this.props.walk.end ? this.props.walk.end : 0,
            poo: this.props.walk.poo ? this.props.walk.poo : 0,
            pooCoordinates: this.props.walk.pooCoordinates ? this.props.walk.pooCoordinates : [],
            owner: this.props.walk.owner,
            duration: null,
            edit: false,
            prevStart: null,
            prevEnd: null
        }
    }

    clickPoo = () => {
        navigator.geolocation.getCurrentPosition(
            position => {
                const latitude = JSON.stringify(position.coords.latitude);
                const longitude = JSON.stringify(position.coords.longitude);

                const pooCoordinates = this.state.pooCoordinates;
                pooCoordinates.push({
                    latitude,
                    longitude
                })

                this.setState({
                    pooCoordinates: pooCoordinates
                });
                this.clickSavePoo();
            },
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
        );
    };

    saveTimeStart = (newTime) => {
        this.setState({ start: newTime.toString() });
    }

    saveTimeStop = (newTime) => {
        this.setState({ end: newTime.toString() });
    }

    cancel = () => {
        this.setState({
            end: this.state.prevEnd,
            start: this.state.prevStart,
            edit: !this.state.edit,
            prevStart: null,
            prevEnd: null
        });
    }

    saveEdit = () => {
        this.setState({
            edit: !this.state.edit,
        });
        this.saveWalk({})
    }

    saveWalk = (updateObject) => {
        this.props.updateWalk({
            ...this.props.walk,
            ...this.state,
            ...updateObject
        }, this.props.indexCurrentWalk)
        //this.props.saveWalks(this.props.walks);
        this.props.saveWalk({
            ...this.props.walk,
            ...this.state,
            ...updateObject
        });
    }

    clickSavePoo = () => {
        if (this.state.poo < 6) {
            const poo = +this.state.poo + 1;
            this.setState({ poo: poo })
            this.saveWalk({ poo: poo })
        }
    }

    clickStart = () => {
        const newDate = new Date().toString();
        this.saveWalk({ start: newDate })
        this.setState({ start: newDate })

        this.interval = setInterval(() => {
            if (this.state.end) {
                clearInterval(this.interval)
            }

            const duration = moment.duration(moment().diff(moment(this.state.start)));
            const seconds = duration.asSeconds();
            this.setState({ duration: Math.round(seconds) });
        }, 20)

    }

    componentDidMount = () => {

        if (this.state.end === 0 && this.state.start !== 0) {
            this.interval = setInterval(() => {

                if (this.state.end) {
                    clearInterval(this.interval)
                }

                const duration = moment.duration(moment().diff(moment(this.state.start)));
                const seconds = duration.asSeconds();
                this.setState({ duration: Math.round(seconds) });
            }, 20)
        } else {
            if (this.state.start !== 0) {
                const duration = moment.duration(moment().diff(moment(this.state.start)));
                const seconds = duration.asSeconds();
                this.setState({ duration: Math.round(seconds) });
            }
        }

    }
    clickEdit = () => {
        this.setState({ edit: !this.state.edit, prevStart: this.state.start, prevEnd: this.state.end })
    }

    clickEnd = () => {
        clearInterval(this.interval)
        const newDate = new Date().toString();
        this.saveWalk({ end: newDate })
        this.setState({ end: newDate })
    }

    getDurationFormatted = (duration) => {

        if (duration >= 3600) {
            return `${Math.floor(duration / 3600)} godzin, ${duration / 60 - (Math.floor(duration / 3600) * 60)} minut, ${duration % 60} sekund `;
        }

        if (duration >= 60) {
            return `${Math.floor(duration / 60)} minut, ${duration % 60} sekund `;
        }

        if (duration === null) {
            return '';
        }
        return `${duration} sekund`;
    }

    render() {

        const items = []
        for (let i = 0; i < this.state.poo; i++) {
            items.push(<FontAwesome5 name="poo" size={25} key={i} />)
        }

        return (
            <Content>
                <Content padder>
                    <Card>
                        <CardItem header button>
                            <Text>Spacer {this.props.currentWalkId}</Text>
                        </CardItem>
                        <CardItem >
                            <Left>
                                <Button iconLeft disabled={this.state.start ? true : false} success={this.state.start ? false : true} block onPress={this.clickStart}>
                                    <Icon name='paw' />
                                    <Text>Start</Text>
                                </Button>
                            </Left>
                            <Right>
                                <Button iconRight failure block onPress={this.clickPoo} disabled={this.state.poo >= 6}>
                                    <FontAwesome5 name='poo' size={25} style={{ color: 'white' }} />
                                </Button>
                            </Right>
                        </CardItem>
                        {!this.state.edit && <CardItem footer button>
                            <Content>
                                <Text>{this.state.start !== 0 && `Start: ${moment(this.state.start).format('hh:mm:ss')}`}</Text>
                                <Text>{this.state.end !== 0 && `Stop: ${moment(this.state.end).format('hh:mm:ss')}`}</Text>
                            </Content>
                            <Right>
                                <Button iconRight warning block onPress={this.clickEdit} disabled={!(this.state.start && this.state.end) ? true : false}>
                                    <FontAwesome5 name='pen' size={25} style={{ color: 'white' }} />
                                    <Text>Edytuj</Text>
                                </Button>
                            </Right>
                        </CardItem>
                        }
                        {this.state.edit &&
                            <>
                                <CardItem>
                                    <TimePicker date={this.state.start} title="Start" saveTime={this.saveTimeStart} />
                                    <TimePicker date={this.state.end} title="Stop" saveTime={this.saveTimeStop} />
                                </CardItem>
                                <CardItem>
                                    <Left >
                                        <Button iconLeft success block onPress={this.saveEdit}>
                                            <Icon name='save' />
                                            <Text>Zapisz</Text>
                                        </Button>
                                    </Left>
                                    <Right >
                                        <Button iconRight danger block onPress={this.cancel}>
                                            <FontAwesome name='remove' size={25} style={{ color: 'white' }} />
                                            <Text>Anuluj</Text>
                                        </Button></Right>
                                </CardItem>
                            </>
                        }
                        <CardItem button>
                            <Content>
                                <Text>{this.state.end !== 0 ? this.getDurationFormatted(Math.round(moment.duration(moment(this.state.end).diff(moment(this.state.start))).asSeconds())) : this.state.duration !== 0 && this.getDurationFormatted(this.state.duration)}</Text>
                            </Content>
                        </CardItem>
                        <CardItem footer button >
                            <Content>
                                <Button iconRight disabled={this.state.end || !this.state.start ? true : false} danger={this.state.end || !this.state.start ? false : true} block onPress={this.clickEnd} >
                                    <FontAwesome5 name='stop-circle' style={{ paddingLeft: 15, fontSize: 20, color: 'white' }} />
                                    <Text>Stop</Text>
                                </Button>
                                <Text style={{ marginTop: 20 }}>{items}</Text>
                            </Content>
                        </CardItem>
                        <CardItem footer button >
                            <Content><Text>Zdobyte punkty na spacerze {this.state.poo}</Text>
                                <Text>
                                    <FontAwesome5 name="poo" size={25} /> {this.state.poo * 100} punktów
                                </Text>
                            </Content>
                        </CardItem>
                    </Card>

                </Content>
            </Content>
        );
    }
}

export default connect(
    ((state) => {

        function isWalkById(walk) {
            return walk.id === state.currentWalkId;
        }
        const currentWalk = state.walks.filter(isWalkById)[0];
        const arrayOfIds = state.walks.map((walk) => walk.id);
        const indexCurrentWalk = arrayOfIds.indexOf(state.currentWalkId);
        return ({
            walk: currentWalk,
            currentWalkId: state.currentWalkId,
            indexCurrentWalk,
            walks: state.walks,
        })
    }), { ...actionCreatorsWalk, ...testActionCreators }
)(Walking);





