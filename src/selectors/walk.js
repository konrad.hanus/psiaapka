export const currentWalkSelector = (state) => {
    function isWalkById(walk) {
        return walk.id === state.currentWalkId;
    }
    return state.walks.filter(isWalkById)[0];
}

export const indexCurrentWalkSelector = (state) => {
    const arrayOfIds = state.walks.map((walk) => walk.id);
    return arrayOfIds.indexOf(state.currentWalkId);
}

export const walksSelector = (state) => {
    return state.walks;
}