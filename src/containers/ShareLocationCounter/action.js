export const DECRERSE_LOCATION_COUNTER = '@@SHARE_LOCATION_COUNTER/DECREASE';
export const INCREASE_LOCATION_COUNTER = '@@SHARE_LOCATION_COUNTER/INCREASE';

export const actionCreators = {
    decreaseLocationCounter: () => {
        return ({
            type: DECRERSE_LOCATION_COUNTER,
        })
    },  
    increaseLocationCounter: () => {
        return ({
            type: INCREASE_LOCATION_COUNTER,
        })
    },  
};