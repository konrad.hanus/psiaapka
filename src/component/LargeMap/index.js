import MapView, { Circle } from "react-native-maps";
import { View, Text, Button, Dimensions } from "react-native";
import { useState, useRef, useEffect } from "react";
import mapStyle from "../mapStyle";
import SCALE from "./../../helpers/scale";

const LargeMap = (props) => {
  useEffect(() => {
      console.log('updated2');
    props.handleZoom(undefined, -65, SCALE-4, 2000);
  }, []);

  const setCurrentUserLocation = async ({ latitude, longitude }) => {

    const camera = await props.mapRef.current.getCamera();
  
    props.mapRef.current.animateCamera(
      {
        ...camera,
        center: {
          latitude: latitude,
          longitude: longitude,
        },
      },
      { duration: 20 }
    );

    console.log("", props.mapRef.current.center);


  };

  const setCurrent = () => {

  };

  const { width: SCREEN_WIDTH, height: SCREEN_HEIGHT } =
    Dimensions.get("window");
    
  return (
    <View>
      <MapView
        ref={props.mapRef}
        style={{
          height: SCREEN_HEIGHT,
          width: SCREEN_WIDTH,
          zIndex: -9991,
        }}
        initialCamera={props.sharedCamera}
        // onUserLocationChange={(e) => {
        //   props.setUserLocation(e.nativeEvent.coordinate);
        // //   setCurrentUserLocation(e.nativeEvent.coordinate);
        // }}
        // onPanDrag={(e) =>
        //   props.handleZoom(e.nativeEvent.position.x, e.nativeEvent.position.y)
        // }
        showsCompass={true}
        provider={MapView.PROVIDER_GOOGLE}
        // customMapStyle={mapStyle}
        // onRegionChange={onRegionChange}
        // onRegionChangeComplete={onRegionChangeComplete}
        zoomEnabled={true}
        scrollEnabled={true}
        showsScale={true}
        rotateEnabled={false}
        followsUserLocation={true}
        showsUserLocation={true}
        mapType={"standard"}
        showsBuildings={false}
      >
        {props.children}
      </MapView>
    </View>
  );
};

export default LargeMap;
