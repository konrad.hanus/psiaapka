import React, { useState, useRef, useEffect } from "react";
import { connect } from "react-redux";
import { actionCreators } from "./MyLocation/action";
import { actionCreators as actionCreatorsFriends } from "./MyFriends/action";
import { actionCreators as actionCreatorsTreasures } from "./Treasure/action";
import { actionCreators as actionCreatorsDogSpots } from "./DogSpot/action";
import { actionCreators as actionCreatorsDogGyms } from "./DogGym/action";

import MapView, { Circle } from "react-native-maps";
import {
  View,
  Text,
  Button,
  Dimensions,
  StyleSheet,
  Pressable,
} from "react-native";
import mapStyle from "../component/mapStyle";
import Bag from "./../component/Bag";
import Cube3D from "../component/3D/Cube3D";
import DDD from "./../component/3D/DDD";
import ModelImport from "./../component/3D/ModelImport";
import Box from "./../component/3D/Box";
import DogSpot from "./../component/3D/DogSpot";
import DogSpot2 from "./../component/3D/DogSpot2";

import MapNew from "./MapNew";

const DogApp = () => {
  const [userLocation, setUserLocation] = useState();
  const [sharedCamera, setSharedCamera] = useState();
  const [rotation, setRotation] = useState(0);
  const [animation, setAnimation] = useState(0);

  const mapRef = useRef(null);

  const handleZoom = async (heading, angle, scale, duration) => {
    setRotation(heading);

    const camera = await mapRef.current.getCamera();

    mapRef.current.animateCamera(
      {
        center: {
          latitude: userLocation.latitude,
          longitude: userLocation.longitude,
        },
        heading: heading ? heading : camera.heading,
        pitch: 10,
        zoom: scale, //? scale : camera.scale,
        // pitch: angle ? (-angle > 20 ? -angle : 15) : camera.angle, //angle
        pitch: 65,
      },
      { duration: duration ? duration : 20 }
    );

    const cameraToSave = await mapRefLarge.current.getCamera();
    setSharedCamera(cameraToSave);
  };

  return (
    <>
      <Bag
        onPress={() => {
          const anim = animation + 1;
          setAnimation(anim);
          //setShowItemInBag(!showItemInBag);
        }}
      />
      <MapNew
        mapRef={mapRef}
        setUserLocation={setUserLocation}
        userLocation={userLocation}
        handleZoom={handleZoom}
        sharedCamera={sharedCamera}
        setSharedCamera={setSharedCamera}
      >
        <MapView.Marker
          coordinate={{ latitude: 51.08641734442424, longitude: 17.0532896713 }}
          onPress={() => alert("Dotknołeś skrzyni ze skarbami jesteś Boski")}
        >
          {/* <Box rotation={rotation} mapRef={mapRef} animation={animation} /> */}
          <DogSpot2 rotation={rotation} mapRef={mapRef} animation={animation} />
        </MapView.Marker>
        <MapView.Marker coordinate={userLocation}>
          <ModelImport
            rotation={rotation}
            mapRef={mapRef}
            animation={animation}
          />
        </MapView.Marker>
      </MapNew>
    </>
  );
};

export default connect(
  (state) => {
    return {
      friends: state.friends,
      locationCounter: state.locationCounter,
      treasures: state.treasures,
      dogSpots: state.dogSpots,
      dogGyms: state.dogGyms,
    };
  },
  {
    setMyLocation: actionCreators.putMyLocation,
    fetchFriendsLocation: actionCreatorsFriends.fetchFriendsLocation,
    fetchTreasuresLocation: actionCreatorsTreasures.fetchTreasuresLocation,
    removeTreasureLocation: actionCreatorsTreasures.removeTreasureLocation,
    removeDogSpotLocation: actionCreatorsDogSpots.removeDogSpotLocation,
    fetchDogsSpotLocation: actionCreatorsDogSpots.fetchDogSpotsLocation,
    removeDogGymLocation: actionCreatorsDogGyms.removeDogGymLocation,
    fetchDogsGymLocation: actionCreatorsDogGyms.fetchDogGymsLocation,
  }
)(DogApp);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
