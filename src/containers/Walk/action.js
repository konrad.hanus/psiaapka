export const CREATE_WALK = '@@WALK/CREATE_WALK';
export const UPDATE_WALK = '@@WALK/UPDATE_WALK';
export const PUT_WALKS = '@@WALK/PUT_WALKS';
export const FETCH_WALKS_DB = '@@WALK/FETCH_WALKS_DB'
export const SAVE_WALKS = '@@WALK/SAVE_WALKS';
export const SAVE_WALK = '@@WALK/SAVE_WALK';
export const REMOVE_WALK = '@@WALK/REMOVE_WALK';
export const SWITCH_WALK = '@@WALK/SWITCH_WALK';

export const actionCreators = {
    createWalk: ({ id, type, ownerId }) => {
        return ({
            type: CREATE_WALK,
            payload:
            {
                id,
                start: 0,
                type,
                end: 0,
                poo: 0,
                owner: ownerId,
            }
        })
    },
    saveWalks: (walks) => {
        return ({
            type: SAVE_WALKS,
            payload: walks
        })
    },

    saveWalk: (walk, userId) => {
        return ({
            type: SAVE_WALK,
            payload: walk,
            meta: userId
        })
    },

    removeWalk: (walk, userId) => {
        return ({
            type: REMOVE_WALK,
            payload: walk,
            meta: userId
        })
    },


    updateWalk: (walk, walkId) => {
        return ({
            type: UPDATE_WALK,
            payload: walk,
            meta: walkId
        })
    },
    putWalks: (walks) => {
        return ({
            type: PUT_WALKS,
            payload: walks,
        })
    },
    fetchWalksDB: () => {
        return ({
            type: FETCH_WALKS_DB,
        })
    },
    switchWalk: (walkId, idCoowner, myId) => {
        return ({
            type: switchWalk,
            payload: { walkId, idCoowner, myId },
        })
    }
};