import React, { useState, useRef, useEffect } from "react";
import { StyleSheet } from "react-native";
import Map2 from "../../component/Map2";
import { NativeBaseProvider } from "native-base";
import ProfileAvatar from "../../component/ProfileAvatar";
import DogAvatar from "../../component/DogAvatar";
import Menu from "../../component/Menu";
import Bag from "../../component/Bag";
import MapView from "react-native-maps";
import Pin from "../../component/Pin";

import { connect } from "react-redux";
import { actionCreators } from "../MyLocation/action";
import { actionCreators as actionCreatorsFriends } from "../MyFriends/action";
import { actionCreators as actionCreatorsTreasures } from "../Treasure/action";
import { actionCreators as actionCreatorsDogSpots } from "../DogSpot/action";
import { actionCreators as actionCreatorsDogGyms } from "../DogGym/action";
import Flare from "../../component/Flare";
import Spadle from "../../component/Spadle";
import AddTreasure from "../../component/AddTreasure";
import AddDogGym from "../../component/AddDogGym";
import AddDogSpot from "../../component/AddDogSpot";
import dogspotImage from "../../component/AddDogSpot/dogspot.png";
import dogGymImage from "../../component/AddDogGym/doggym.png";
import ModelImport from "../../component/ModelImport";

const DogApp = (props) => {
  const [page, setPage] = useState("main");
  const [showFlare, setShowFlare] = useState(false);
  const [showItemInBag, setShowItemInBag] = useState(false);

  const [builder, setBuilder] = useState(false);

  const fireFlare = () => {
    setShowFlare(true);

    setTimeout(() => {
      setShowFlare(false);
    }, 30000);
  };
  const mapRef = useRef(null);
  const mapRefLarge = useRef(null);
  const [userLocation, setUserLocation] = useState();
  const [sharedCamera, setSharedCamera] = useState();

  const handleZoom = async (heading, angle, scale, duration) => {
    const camera = await mapRef.current.getCamera();

    mapRef.current.animateCamera(
      {
        center: {
          latitude: userLocation.latitude,
          longitude: userLocation.longitude,
        },
        heading: heading ? heading : camera.heading,
        pitch: 10,
        zoom: scale, //? scale : camera.scale,
        // pitch: angle ? (-angle > 20 ? -angle : 15) : camera.angle, //angle
        pitch: 65,
      },
      { duration: duration ? duration : 20 }
    );

    const cameraToSave = await mapRefLarge.current.getCamera();
    setSharedCamera(cameraToSave);
  };

  const handleZoomLargeClick = async (heading, angle, scale, duration) => {
    const cameraLarge = await mapRefLarge.current.getCamera();
    mapRefLarge.current.animateCamera(
      {
        center: {
          latitude: userLocation.latitude,
          longitude: userLocation.longitude,
        },
        heading: heading ? heading : cameraLarge.heading,
        zoom: scale, //? scale : camera.scale,
        // pitch: angle ? (-angle > 20 ? -angle : 15) : camera.angle, //angle
        pitch: 0,
      },
      { duration: duration ? duration : 20 }
    );
  };

  const handleZoomLarge = async (heading, angle, scale, duration) => {
    const cameraToSave = await mapRefLarge.current.getCamera();
    setSharedCamera(cameraToSave);
  };

  const coordinate = [
    { latitude: 51.086781129189376, longitude: 17.05304514781193 },
    { latitude: 51.086781119189376, longitude: 17.05304513781193 },
    { latitude: 51.086781139189376, longitude: 17.05304515781193 },
    { latitude: 51.086781119189376, longitude: 17.05304515781193 },
  ];

  useEffect(() => {
    console.log("only oncee");
  }, []);
  // useEffect(()=>{
  //   props.fetchFriendsLocation();
  // })

  const DogSpotSComponent =
    props.dogSpots &&
    Object.keys(props.dogSpots).map((key, index) => {
      const location = props.dogSpots[key].location;
      return (
        <MapView.Marker
          onPress={() => {
            props.removeDogSpotLocation(key);
            // alert('To jest skarb, dodany przez '+ props.treasures[key].who);
          }}
          coordinate={location}
        >
          {
            <Pin
              image={dogspotImage}
              onPress={() => {
                alert("profil Psa");
              }}
            />
          }
        </MapView.Marker>
      );
    });

  const DogGymsComponent =
    props.dogGyms &&
    Object.keys(props.dogGyms).map((key, index) => {
      const location = props.dogGyms[key].location;
      return (
        <MapView.Marker
          onPress={() => {
            props.removeDogGymLocation(key);
            // alert('To jest skarb, dodany przez '+ props.treasures[key].who);
          }}
          coordinate={location}
        >
          {
            <Pin
              image={dogGymImage}
              onPress={() => {
                alert("profil Psa");
              }}
            />
          }
        </MapView.Marker>
      );
    });

  const TreasuresComponent =
    props.treasures &&
    Object.keys(props.treasures).map((key, index) => {
      const location = props.treasures[key].location;
      return (
        <MapView.Marker
          onPress={() => {
            props.removeTreasureLocation(key);
            // alert('To jest skarb, dodany przez '+ props.treasures[key].who);
          }}
          coordinate={location}
        >
          {/* <DDD /> */}
          {/* {
        <Pin
          image={treasure}
          onPress={() => {
            alert("profil Psa");
          }}
        />
      } */}
        </MapView.Marker>
      );
    });

  return (
    <NativeBaseProvider>
      {/* <SafeAreaView> */}
      {page === "main" ? (
        <>
          {/* <Gestures handleZoom={handleZoom}> */}
          <>
            {builder && (
              <>
                <AddDogGym
                  onPress={() => {
                    props.setMyLocation(userLocation, "dogGym");
                    props.fetchDogsGymLocation();
                    alert("Dodano tutaj dog gym'a");
                  }}
                />
                <AddDogSpot
                  onPress={() => {
                    props.setMyLocation(userLocation, "dogSpot");
                    props.fetchDogsSpotLocation();
                    alert("Dodano tutaj dogSpot'a");
                  }}
                />
                <AddTreasure
                  onPress={() => {
                    props.setMyLocation(userLocation, "treasure");
                    props.fetchTreasuresLocation();
                    alert("Zakopano tutaj skarb");
                  }}
                />
              </>
            )}
            <ProfileAvatar
              onPress={() => {
                setBuilder(!builder);
              }}
              distance={0}
              speed={0}
            />
            <DogAvatar
              onPress={() => {
                setBuilder(!builder);
              }}
            />
            <Menu
              label={"MAPA"}
              onPress={() => {
                props.fetchFriendsLocation();
                setPage("2");
              }}
            />
            {showItemInBag && (
              <>
                <Spadle
                  onPress={() => {
                    alert(
                      "Uzyj szpadla w miejscu gdzie bedziesz chciał odkopać skarb"
                    );
                  }}
                />

                {props.locationCounter !== 0 && (
                  <Flare
                    onPress={() => {
                      if (!showFlare) {
                        props.setMyLocation(userLocation);
                        fireFlare();
                      }
                    }}
                  />
                )}
              </>
            )}
            <Bag
              onPress={() => {
                setShowItemInBag(!showItemInBag);
              }}
            />
          </>
          {/* </Gestures> */}
          <Map2
            mapRef={mapRef}
            setUserLocation={setUserLocation}
            userLocation={userLocation}
            handleZoom={handleZoom}
            sharedCamera={sharedCamera}
            setSharedCamera={setSharedCamera}
          >
            <MapView.Marker coordinate={userLocation}>
              {/* {
              <>{showFlare && <Image
                style={{
                  width: 90,
                  height: 70,
                  resizeMode: 'stretch',
                }}
                source={flare2}
              ></Image>}
                <Pin
                  onPress={() => {
                    alert("profil Psa");
                  }}
                />
              </>
            } */}
              <ModelImport />
            </MapView.Marker>
            {/* <MapView.Marker
            onPress={() => {
              alert("profil Psa");
            }}
            coordinate={{
              accuracy: 7.277932641796032,
              altitude: 126.86895075347275,
              altitudeAccuracy: 2.3041577283365453,
              heading: -1,
              latitude: 51.08600151821038,
              longitude: 17.052081253977353,
              speed: 0,
              timestamp: 672945654001.0166,
            }}
          >
            {
              <Pin
                image={Doge}
                onPress={() => {
                  alert("profil Psa");
                }}
              />
            }
          </MapView.Marker> */}

            {DogSpotSComponent}
            {TreasuresComponent}
            {DogGymsComponent}
          </Map2>
        </>
      ) : (
        <>
          <>
            <ProfileAvatar
              onPress={() => {
                alert("Profil Pana");
              }}
              distance={0}
              speed={0}
            />
            <DogAvatar
              onPress={() => {
                alert("profil psa");
              }}
            />
            <Menu
              label={"Gra"}
              onPress={() => {
                props.fetchDogsGymLocation();
                props.fetchDogsSpotLocation();
                props.fetchTreasuresLocation();
                setPage("main");
              }}
            />
            <Bag
              onPress={() => {
                alert("Tutaj będzie to co znajdziesz, jak cos znajdziesz");
              }}
            />
          </>
          {/* <LargeMap
            mapRef={mapRefLarge}
            setUserLocation={setUserLocation}
            userLocation={userLocation}
            handleZoom={handleZoomLarge}
            sharedCamera={sharedCamera}
            setSharedCamera={setSharedCamera}
          >
            

            {DogSpotSComponent}
            {TreasuresComponent}
            {DogGymsComponent}

            {props.friends &&
              Object.keys(props.friends).map((key, index) => {
                const location =
                  props.friends[key][Object.keys(props.friends[key])[0]]
                    .location;
                return (
                  <MapView.Marker
                    onPress={() => {
                      alert(key);
                    }}
                    coordinate={location}
                  >
                    {
                      <Pin
                        image={Doge}
                        onPress={() => {
                          alert("profil Psa");
                        }}
                      />
                    }
                  </MapView.Marker>
                );
              })}
          </LargeMap> */}
        </>
      )}
    </NativeBaseProvider>
  );
};

export default connect(
  (state) => {
    return {
      friends: state.friends,
      locationCounter: state.locationCounter,
      treasures: state.treasures,
      dogSpots: state.dogSpots,
      dogGyms: state.dogGyms,
    };
  },
  {
    setMyLocation: actionCreators.putMyLocation,
    fetchFriendsLocation: actionCreatorsFriends.fetchFriendsLocation,
    fetchTreasuresLocation: actionCreatorsTreasures.fetchTreasuresLocation,
    removeTreasureLocation: actionCreatorsTreasures.removeTreasureLocation,
    removeDogSpotLocation: actionCreatorsDogSpots.removeDogSpotLocation,
    fetchDogsSpotLocation: actionCreatorsDogSpots.fetchDogSpotsLocation,
    removeDogGymLocation: actionCreatorsDogGyms.removeDogGymLocation,
    fetchDogsGymLocation: actionCreatorsDogGyms.fetchDogGymsLocation,
  }
)(DogApp);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
