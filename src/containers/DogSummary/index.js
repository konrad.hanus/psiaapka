import React from 'react';
import { Text, Button, Thumbnail, Body, Row, Badge } from 'native-base';
import exampleImage from './parkbackground.jpg';
import { StyleSheet, View, ImageBackground, Animated } from 'react-native';
import { connect } from 'react-redux';
import { actionCreators } from '../Walk/action';
import Constants from 'expo-constants';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

export const DogSummary = () => {

    const uri = "https://firebasestorage.googleapis.com/v0/b/psiaapka.appspot.com/o/23456194_132103257554483_7304443404378693833_o.jpg?alt=media&token=e77f3678-c87c-4eed-a85c-942dcc374e31";

    return (
        <View style={styles.container}>
            <ImageBackground source={exampleImage} style={styles.image}>
                <Body style={{ zIndex: 10 }}><Text style={{ color: "white", fontWeight: "bold", fontSize: 25 }}>Poziom 1</Text>
                    <Thumbnail large source={{ uri: uri }} style={{ width: 180, height: 180, borderRadius: 100, marginTop: 10, }} />
                     <FontAwesome5 name={"pen"} size={30} color={"white"} style={{marginTop: -45, marginRight: -100}}/>
                </Body>
                <View style={styles.container2}>
                    <Body>

                        <Text style={{ fontSize: 26 }}>Figa
                            {/* <FontAwesome5 name={"pen"} size={18} color={"black"} /> */}
                        </Text>
                        <Text style={{ fontSize: 14 }}>Konrad & Krysia</Text>
                        <View style={styles.progressBarContainer}>
                            <View style={styles.progressBar}>
                                <Animated.View style={{ backgroundColor: "#32cd32", width: 271 }} />
                            </View>
                            <Text>725/1000 XP</Text>
                        </View>
                        <Row>
                            <Text style={{ fontSize: 22 }}>     12kg       8 lat     Dolnośląska </Text>
                        </Row>
                        <Row>
                            <Text>Waga        Wiek              Rasa      </Text>
                        </Row>
                        <Row>
                            <Text> </Text>
                        </Row>
                        <Row>
                            <Text></Text>
                        </Row>
                        
                        <Body>
                            <Button>
                                <Text>Dodaj nową sztuczkę</Text>
                            </Button>
                        </Body>
                        <Row style={{ marginTop: 20 }}>
                            <Text>Odznaki</Text>
                        </Row>
                        <Row>

                            <Badge warning>
                                <IconFontAwesome name={"tree"} size={20} color={"green"} />
                            </Badge>

                            <Badge warning>
                                <IconFontAwesome name={"heartbeat"} size={20} color={"green"} />
                            </Badge>

                            <Badge warning>
                                <IconFontAwesome name={"angellist"} size={20} color={"green"} />
                            </Badge>

                            <Badge warning>
                                <IconFontAwesome name={"connectdevelop"} size={20} color={"green"} />
                            </Badge>

                            <Badge warning>
                                <IconFontAwesome name={"ship"} size={20} color={"green"} />
                            </Badge>

                            <Badge warning>
                                <IconFontAwesome name={"map-o"} size={20} color={"green"} />
                            </Badge>
                        </Row>
                    </Body>
                </View>

            </ImageBackground>
        </View>
    )
}


export default connect(
    ((state) => {
        return ({
            walks: state.walks
        })
    }), (actionCreators)
)(DogSummary);

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    container2: {
        flex: 1,
        backgroundColor: "white",
        borderRadius: 100,
        paddingBottom: 200,
        marginBottom: -150,
        marginTop: -30,
        zIndex: 1,
        paddingTop: 50,
        marginLeft: 10,
        marginRight: 10
    },
    image: {
        flex: 1,
        resizeMode: "cover",
        justifyContent: "center"
    },
    progressBarContainer: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: Constants.statusBarHeight,
        padding: 8,
    },
    progressBar: {
        flexDirection: 'row',
        height: 20,
        width: 300,
        backgroundColor: 'white',
        borderColor: '#000',
        borderWidth: 2,
        borderRadius: 5
    },
    absoluteFill: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0
    },
    text: {
        fontSize: 42,
        fontWeight: "bold",
        textAlign: "center",
    }
});
