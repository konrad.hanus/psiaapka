import { ExpoWebGLRenderingContext, GLView } from "expo-gl";
import { Renderer, TextureLoader } from "expo-three";
import * as React from "react";
import { View } from "react-native";
import {
  AmbientLight,
  BoxBufferGeometry,
  OctahedronBufferGeometry,
  OctahedronGeometry,
  Fog,
  GridHelper,
  Mesh,
  MeshStandardMaterial,
  PerspectiveCamera,
  PointLight,
  Scene,
  SpotLight,
  AnimationMixer,
  Clock,
} from "three";
import { GLTFLoader } from "../TreeExample/GLTFLoader";

FileReader.prototype.readAsArrayBuffer = function (blob) {
  if (this.readyState === this.LOADING) throw new Error("InvalidStateError");
  this._setReadyState(this.LOADING);
  this._result = null;
  this._error = null;
  const fr = new FileReader();
  fr.onloadend = () => {
    const content = atob(
      fr.result.substr("data:application/octet-stream;base64,".length)
    );
    const buffer = new ArrayBuffer(content.length);
    const view = new Uint8Array(buffer);
    view.set(Array.from(content).map((c) => c.charCodeAt(0)));
    this._result = buffer;
    this._setReadyState(this.DONE);
  };
  fr.readAsDataURL(blob);
};

// from: https://stackoverflow.com/questions/42829838/react-native-atob-btoa-not-working-without-remote-js-debugging
const chars =
  "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
const atob = (input = "") => {
  let str = input.replace(/=+$/, "");
  let output = "";

  if (str.length % 4 == 1) {
    throw new Error(
      "'atob' failed: The string to be decoded is not correctly encoded."
    );
  }
  for (
    let bc = 0, bs = 0, buffer, i = 0;
    (buffer = str.charAt(i++));
    ~buffer && ((bs = bc % 4 ? bs * 64 + buffer : buffer), bc++ % 4)
      ? (output += String.fromCharCode(255 & (bs >> ((-2 * bc) & 6))))
      : 0
  ) {
    buffer = chars.indexOf(buffer);
  }

  return output;
};

export default function App(props) {
  let timeout;

  React.useEffect(() => {
    // Clear the animation loop when the component unmounts
    return () => clearTimeout(timeout);
  }, []);
  const YYY = props.mapRef;
  return (
    <View
      style={{
        width: 300,
        height: 300,
      }}
    >
      <GLView
        style={{
          width: 300,
          height: 300,
          // borderWidth: 2,
          borderColor: "red",
        }}
        onContextCreate={async (gl) => {
          const { drawingBufferWidth: width, drawingBufferHeight: height } = gl;
          const sceneColor = 0xff0000;

          // Create a WebGLRenderer without a DOM element
          const renderer = new Renderer({ gl });
          renderer.setSize(width, height);
          // renderer.setClearColor(sceneColor);

          const camera = new PerspectiveCamera(20, width / height, 0.01, 1000);
          camera.position.set(9, 6, -5);
          camera.rotation.set(-122.93, 50.42, 130.25);

          const scene = new Scene();
          // scene.fog = new Fog(sceneColor, 1, 10000);
          // scene.add(new GridHelper(10, 10));

          const ambientLight = new AmbientLight(0x101010);
          scene.add(ambientLight);

          const pointLight = new PointLight(0xffffff, 2, 1000, 1);
          pointLight.position.set(20, 20, 0);
          scene.add(pointLight);

          // const pointLight2 = new PointLight(0xffffff, 2, 1000, 1);
          // pointLight2.position.set(0, 200, 0);
          // scene.add(pointLight2);

          const spotLight = new SpotLight(0xffffff, 0.5);
          spotLight.position.set(0, 500, 100);
          spotLight.lookAt(scene.position);
          scene.add(spotLight);

          const cube = new IconMesh();

          // scene.add(cube);

          camera.lookAt(cube.position);

          const loader = new GLTFLoader();
          let mixer;
          let person;

          let stacy_txt = new TextureLoader().load(
            "https://s3-us-west-2.amazonaws.com/s.cdpn.io/1376484/stacy.jpg"
          );

          stacy_txt.flipY = false;

          loader.load(
            "https://ghggroup.nazwa.pl/1/stacy_lightweight.glb",
            function (glb) {
              // console.log(glb);
              person = glb.scene;
              mixer = new AnimationMixer(person);
              const clips = glb.animations;
              const action = mixer.clipAction(clips[6]);
              action.play();
              // const clip = AnimationClip.findByName(clips)
              // mixer.clipAction(person.animations[0]).play();
              // console.log(glb.scene.children[0])
              person.position.set(2.5, 0, -1.5);
              person.rotation.set(0, 0, 0);
              person.set;
              scene.add(person);

              // camera.lookAt(person.position);
            },
            function (xhr) {
              // console.log((xhr.loader/xhr.total * 100) + "% loaded");
            },
            function (error) {
              // console.log('error', error);
            }
          );

          async function update(rot, person) {
            const camera = await YYY.current.getCamera();
            // console.log("camera", camera.heading / 50 + 1.5);
            person.rotation.y = camera.heading / 50 + 1.5;
          }

          // Setup an animation loop
          const clock = new Clock();
          const render = () => {
            timeout = requestAnimationFrame(render);
            update(props.rotation, person);
            mixer && mixer.update(clock.getDelta());
            renderer.render(scene, camera);
            gl.endFrameEXP();
          };
          render();
        }}
      />
    </View>
  );
}

class IconMesh extends Mesh {
  constructor() {
    super(
      new OctahedronGeometry(1.0, 0),
      // new BoxBufferGeometry(1.0, 1.0, 1.0),
      new MeshStandardMaterial({
        // map: new TextureLoader().load(require("./assets/icon.png")),
        color: 0xff0000,
      })
    );
  }
}
