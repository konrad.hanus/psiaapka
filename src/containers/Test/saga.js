import { takeEvery, call, all, fork } from 'redux-saga/effects';
import { FIRST_TEST_SAGA } from './action';
import firebase from 'firebase';
import ReduxSagaFirebase from 'redux-saga-firebase';
import apiKeys from '../../constants/apiKeys';


const myFirebaseApp =  !firebase.apps.length ? firebase.initializeApp(apiKeys.FirebaseConfig) : firebase.app();
const reduxSagaFirebase = new ReduxSagaFirebase(myFirebaseApp);

function* onFirstTestSaga() {
   // console.log('z sagi');
    yield call(reduxSagaFirebase.database.create, '/walks', 'test');
}


function* firstTestSaga() {
   // console.log('test22');
    yield takeEvery(FIRST_TEST_SAGA, onFirstTestSaga);
    //console.log('test2233');
}

export function* firstTest() {
    yield all([fork(firstTestSaga)]);
}
   