import React from "react";
import Expo from "expo";
import {
  Scene,
  Mesh,
  MeshBasicMaterial,
  PerspectiveCamera,
  BoxBufferGeometry,
} from "three";
import ExpoTHREE, { Renderer } from "expo-three";
import { ExpoWebGLRenderingContext, GLView } from "expo-gl";
import { View, StyleSheet, SafeAreaView } from "react-native";

import { Provider } from "react-redux";
import configStore, { sagaMiddleware } from "./store";
import initSagas from "./initSagas";
// import { connect } from "react-redux";
// import { actionCreators } from "./src/containers/MyLocation/action";
import DogApp from "./src/containers/";

export const store = configStore();
initSagas(sagaMiddleware);

export default App = (props) => {
  return (
    <Provider store={store}>
      <DogApp />
    </Provider>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
